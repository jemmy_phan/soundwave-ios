# Soundwave

[![CI Status](http://img.shields.io/travis/jemmyphan/Soundwave.svg?style=flat)](https://travis-ci.org/jemmyphan/Soundwave)
[![Version](https://img.shields.io/cocoapods/v/Soundwave.svg?style=flat)](http://cocoapods.org/pods/Soundwave)
[![License](https://img.shields.io/cocoapods/l/Soundwave.svg?style=flat)](http://cocoapods.org/pods/Soundwave)
[![Platform](https://img.shields.io/cocoapods/p/Soundwave.svg?style=flat)](http://cocoapods.org/pods/Soundwave)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Soundwave is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Soundwave'
```

## Author

jemmyphan, jemmyphan94@gmail.com

## License

Soundwave is available under the MIT license. See the LICENSE file for more info.
