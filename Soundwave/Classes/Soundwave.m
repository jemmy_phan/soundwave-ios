//
//  Soundwave.m
//  Soundwave
//
//  Created by Jemmy phan on 20/10/17.
//

#import <Foundation/Foundation.h>

#import "Soundwave.h"

#import <AVFoundation/AVFoundation.h>
#include <AudioToolbox/AudioSession.h>

#if !(TARGET_OS_SIMULATOR)
#import "MyPcmPlayerImp.h"
#import "MyPcmRecorderImp.h"
#endif

static const char* const CODE_BOOK = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_|";
#define TOKEN_COUNT 24

FILE* mFile;

#if !(TARGET_OS_SIMULATOR)
ESVoid onSinVoiceRecognizerStart(ESVoid* cbParam) {
    NSLog(@"soundwave listen started");
    Soundwave* sw = (__bridge Soundwave*)cbParam;
    sw->mResultCount = 0;
}

ESVoid onSinVoiceRecognizerToken(ESVoid* cbParam, ESInt32 index) {
    NSLog(@"soundwave listen catched, index:%d", index);
    Soundwave* sw = (__bridge Soundwave*)cbParam;
    sw->mResults[sw->mResultCount++] = index;
}

ESVoid onSinVoiceRecognizerEnd(ESVoid* cbParam, ESInt32 result) {
    NSLog(@"soundwave listen finished, result:%d", result);
    Soundwave* sw = (__bridge Soundwave*)cbParam;
    [sw onRecogToken:sw];
}

ESVoid onSinVoicePlayerStart(ESVoid* cbParam) {
    NSLog(@"soundwave start playing");
    Soundwave* sw = (__bridge Soundwave*)cbParam;
    [sw onPlayData:sw];
    NSLog(@"soundwave finished playing");
}

ESVoid onSinVoicePlayerStop(ESVoid* cbParam) {
    NSLog(@"soundwave stop playing");
}

SinVoicePlayerCallback mPlayerCallback = {onSinVoicePlayerStart, onSinVoicePlayerStop};
SinVoiceRecognizerCallback mReceiverCallback = {onSinVoiceRecognizerStart, onSinVoiceRecognizerToken, onSinVoiceRecognizerEnd};
#endif

@implementation Soundwave

@synthesize receive;

- (instancetype)init
{
    self = [super init];
    if (self) {

#if !(TARGET_OS_SIMULATOR)
        mPcmPlayer.create = MyPcmPlayerImp_create;
        mPcmPlayer.start = MyPcmPlayerImp_start;
        mPcmPlayer.stop = MyPcmPlayerImp_stop;
        mPcmPlayer.setParam = MyPcmPlayerImp_setParam;
        mPcmPlayer.destroy = MyPcmPlayerImp_destroy;
        mSinVoicePlayer = SinVoicePlayer_create2("com.sinvoice.demo", "SinVoiceDemo", &mPlayerCallback, (__bridge ESVoid *)(self), &mPcmPlayer);
        mMaxEncoderIndex = SinVoicePlayer_getMaxEncoderIndex(mSinVoicePlayer);
#endif
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];

    }
    return self;
}

- (void) listen:(void(^)(NSString*)) callback
{
#if !(TARGET_OS_SIMULATOR)
    if (!mSinVoiceRecorder) {
        mPcmRecorder.create = MyPcmRecorderImp_create;
        mPcmRecorder.start = MyPcmRecorderImp_start;
        mPcmRecorder.stop = MyPcmRecorderImp_stop;
        mPcmRecorder.setParam = MyPcmRecorderImp_setParam;
        mPcmRecorder.destroy = MyPcmRecorderImp_destroy;
        mSinVoiceRecorder = SinVoiceRecognizer_create2("com.sinvoice.demo", "SinVoiceDemo", &mReceiverCallback, (__bridge ESVoid *)(self), &mPcmRecorder);
    }

    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    receive = callback;
    SinVoiceRecognizer_start(mSinVoiceRecorder, TOKEN_COUNT);
    mFile = fopen([NSHomeDirectory() stringByAppendingPathComponent:@"Documents/record1.pcm"].UTF8String, "wb");
#endif
}

- (void) stopListen
{
#if !(TARGET_OS_SIMULATOR)
    SinVoiceRecognizer_stop(mSinVoiceRecorder);
    if ( ES_NULL != mFile ) {
        fclose(mFile);
        mFile = ES_NULL;
    }
#endif
}

- (void) transmit:(NSString *)message
{
#if !(TARGET_OS_SIMULATOR)
    NSMutableString *value = [message mutableCopy];
    value = [[value stringByReplacingOccurrencesOfString:@" " withString:@"|"] mutableCopy];
    
    if ([value length] > 25) {
        value = [[value substringToIndex:25] mutableCopy];
    }
    const char* str = [value cStringUsingEncoding:NSUTF8StringEncoding];
    
    mPlayCount = (int)strlen(str);
    
    int index = 0;
    if ( mMaxEncoderIndex < 255 ) {
        int lenCodeBook = (int)strlen(CODE_BOOK);
        int isOK = 1;
        while ( index < mPlayCount) {
            int i = 0;
            for ( i = 0; i < lenCodeBook; ++i ) {
                if ( str[index] == CODE_BOOK[i] ) {
                    mRates[index] = i;
                    break;
                }
            }
            if ( i >= lenCodeBook ) {
                isOK = 0;
                break;
            }
            ++index;
        }
        if ( isOK ) {
            SinVoicePlayer_play(mSinVoicePlayer, mRates, mPlayCount);
        }
    } else {
        int index = 0;
        
        while ( index < mPlayCount) {
            mRates[index] = str[index];
            ++index;
        }
        SinVoicePlayer_play(mSinVoicePlayer, mRates, mPlayCount);
    }
#endif
}

- (void) stopTransmit
{
#if !(TARGET_OS_SIMULATOR)
    SinVoicePlayer_stop(mSinVoicePlayer);
#endif
}

- (void) destroy
{
    [self stopTransmit];
    [self stopListen];
}

- (void) onRecogToken:(Soundwave *)data
{
    NSString* value;
    if ( mMaxEncoderIndex < 255 ) {
        NSMutableString* bufferString = [[NSMutableString alloc]init];
        for ( int i = 0; i < mResultCount; ++i ) {
            [bufferString appendFormat:@"%c", CODE_BOOK[data->mResults[i]]];
        }
        value = (NSString *) bufferString;
    } else {
        char ch[100] = { 0 };
        for ( int i = 0; i < mResultCount; ++i ) {
            ch[i] = (char)data->mResults[i];
        }
        value = [NSString stringWithCString:ch encoding:NSUTF8StringEncoding];
    }
    
    if (receive) {
        receive([value stringByReplacingOccurrencesOfString:@"|" withString:@" "]);
    }
}

- (void) onPlayData:(Soundwave *)data
{
    
}
@end


