//
//  Soundwave.h
//  Pods
//
//  Created by Jemmy phan on 20/10/17.
//

#ifndef Soundwave_h
#define Soundwave_h

#if !(TARGET_IPHONE_SIMULATOR)

#import "SinVoicePlayer.h"
#import "SinVoiceRecognizer.h"
#include "ESPcmPlayer.h"
#include "ESPcmRecorder.h"

#endif

@class Soundwave;

@interface Soundwave : NSObject
{
    
#if !(TARGET_IPHONE_SIMULATOR)
@private
    SinVoicePlayer*     mSinVoicePlayer;
    SinVoiceRecognizer* mSinVoiceRecorder;
    ESPcmPlayer         mPcmPlayer;
    ESPcmRecorder       mPcmRecorder;
#endif
    
@public
    int mRates[100];
    int mPlayCount;
    int mResults[100];
    int mResultCount;
    int mMaxEncoderIndex;
    
}

@property (nonatomic, copy) void (^receive)(NSString *value);

- (instancetype)init;
- (void)onPlayData:(Soundwave *)data;
- (void)onRecogToken:(Soundwave *)data;

- (void)listen:(void (^)(NSString*)) callback;
- (void)stopListen;
- (void)transmit:(NSString *)message;
- (void)stopTransmit;
- (void)destroy;

@end

#endif /* Soundwave_h */

