#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "ESPcmPlayer.h"
#import "ESPcmRecorder.h"
#import "ESType.h"
#import "MyPcmPlayerImp.h"
#import "MyPcmRecorderImp.h"
#import "SinVoicePlayer.h"
#import "SinVoiceRecognizer.h"
#import "Soundwave.h"

FOUNDATION_EXPORT double SoundwaveVersionNumber;
FOUNDATION_EXPORT const unsigned char SoundwaveVersionString[];

