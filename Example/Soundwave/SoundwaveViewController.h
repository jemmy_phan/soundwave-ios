//
//  SoundwaveViewController.h
//  Soundwave
//
//  Created by jemmyphan on 10/19/2017.
//  Copyright (c) 2017 jemmyphan. All rights reserved.
//

@import UIKit;
@import Soundwave;

@interface SoundwaveViewController : UIViewController
{
}

@property (retain, nonatomic) Soundwave *soundwave;
@property (weak, nonatomic) IBOutlet UITextField *messageTextField;
@property (weak, nonatomic) IBOutlet UITextView *transmitLabel;

@end
