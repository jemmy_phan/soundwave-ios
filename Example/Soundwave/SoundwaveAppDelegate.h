//
//  SoundwaveAppDelegate.h
//  Soundwave
//
//  Created by jemmyphan on 10/19/2017.
//  Copyright (c) 2017 jemmyphan. All rights reserved.
//

@import UIKit;

@interface SoundwaveAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
