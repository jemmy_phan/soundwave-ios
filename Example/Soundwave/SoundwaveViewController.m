//
//  SoundwaveViewController.m
//  Soundwave
//
//  Created by jemmyphan on 10/19/2017.
//  Copyright (c) 2017 jemmyphan. All rights reserved.
//

#import "SoundwaveViewController.h"

@interface SoundwaveViewController ()

@end

@implementation SoundwaveViewController

@synthesize soundwave;

- (void)viewDidLoad
{
    [super viewDidLoad];
    soundwave = [[Soundwave alloc] init];
}

- (IBAction)transmit:(UIButton *)sender {
    [soundwave transmit:_messageTextField.text];
}

- (IBAction)listen:(UIButton *)sender {
    [soundwave listen
     :^(NSString *value) {
         dispatch_async(dispatch_get_main_queue(), ^{
             _transmitLabel.text = value;
         });
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
